package uk.co.egao;

import java.util.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * Shopping cart representation
 */
public class ShoppingCart {
    private Map<String, Long> items;

    public ShoppingCart() {
        this.items = new HashMap<>();
    }

    public ShoppingCart(List<String> items) {
        this.items = items.stream().collect(groupingBy(identity(), counting()));
    }

    public ShoppingCart(String... items) {
        this(Arrays.asList(items));
    }

    public void addItem(String name, long quantity) {
        assert quantity >= 0;
        items.put(name, items.getOrDefault(name, 0L) + quantity);
    }

    public void removeItem(String name, long quantity) {
        assert quantity >= 0;
        items.put(name, Math.max(0, items.getOrDefault(name, 0L) - quantity));
    }

    /**
     * Get copy of the shopping cart items
     *
     * @return
     */
    public Map<String, Long> getItems() {
        return Collections.unmodifiableMap(items);
    }
}
