package uk.co.egao;

import java.util.Collections;
import java.util.Map;

/**
 * Simple discount representation. Suitable for describing discounts similar to
 * 'buy two get one free' or 'two for price of one'.
 * <p>
 * Could be easily extended to provide fixed value or percent of total price type of discounts.
 */
public class Discount {
    private String description;
    private Map<String, Long> condition;
    private Map<String, Long> freeItems;

    public Discount(String description, Map<String, Long> condition, Map<String, Long> freeItems) {
        if (condition.isEmpty() || freeItems.isEmpty()) {
            throw new IllegalArgumentException("Undefined discount conditions");
        }

        if (!condition.keySet().containsAll(freeItems.keySet())) {
            throw new IllegalArgumentException("Free items should be selected from the discount bundle items");
        }

        for (String item : freeItems.keySet()) {
            if (condition.getOrDefault(item, 0L) < freeItems.get(item)) {
                throw new IllegalArgumentException("Number of free items should not exceed bundle size");
            }
        }

        this.description = description;
        this.condition = Collections.unmodifiableMap(condition);
        this.freeItems = Collections.unmodifiableMap(freeItems);
    }

    /**
     * Checks if the discount applies to the provided cart items
     *
     * @param items shopping cart items
     * @return true if the discount applies
     */
    public boolean hasMatch(Map<String, Long> items) {
        for (Map.Entry<String, Long> requiredItem : condition.entrySet()) {
            if (items.getOrDefault(requiredItem.getKey(), 0L) < requiredItem.getValue()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Modifies the provided items map removing discount bundle items
     *
     * @param items shopping cart items map
     * @return true if items were removed from cart
     */
    public boolean removeMatch(Map<String, Long> items) {
        if (hasMatch(items)) {
            for (Map.Entry<String, Long> requiredItem : condition.entrySet()) {
                items.put(
                        requiredItem.getKey(),
                        Math.max(
                                0,
                                items.getOrDefault(requiredItem.getKey(), 0L) - requiredItem.getValue()
                        )
                );
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets free items for this discount bundle
     *
     * @return item name to quantity map
     */
    public Map<String, Long> getFreeItems() {
        return freeItems;
    }

    public String getDescription() {
        return description;
    }
}
