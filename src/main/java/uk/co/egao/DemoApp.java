package uk.co.egao;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;

import static java.util.Collections.singletonMap;

/**
 * Demo shopping cart application. Assumes that items are supplied as application arguments.
 * Supported item names: Apple, Banana, Lime, Melon
 */
public class DemoApp {
    public static void main(String[] args) {
        // add arguments as items to the shopping cart
        ShoppingCart cart = new ShoppingCart(args);

        // add preconfigured prices
        HashMap<String, BigDecimal> prices = new HashMap<>();
        prices.put("Apple", BigDecimal.valueOf(0.35));
        prices.put("Banana", BigDecimal.valueOf(0.2));
        prices.put("Melon", BigDecimal.valueOf(0.5));
        prices.put("Lime", BigDecimal.valueOf(0.15));

        // add discounts
        CartPrice cartPrices = new CartPrice(prices, Arrays.asList(
                new Discount("Melons - buy one get one free",
                        singletonMap("Melon", 2L), singletonMap("Melon", 1L)
                ),
                new Discount("Limes - three for the price of two",
                        singletonMap("Lime", 2L), singletonMap("Lime", 1L)
                )
        ));

        // print cart price
        System.out.printf("Cart price: %.2f\n", cartPrices.getPrice(cart));
    }
}
