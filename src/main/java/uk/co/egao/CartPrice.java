package uk.co.egao;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartPrice {
    private Map<String, BigDecimal> prices;
    private List<Discount> discounts;

    public CartPrice(Map<String, BigDecimal> prices, List<Discount> discounts) {
        this.prices = Collections.unmodifiableMap(prices);
        this.discounts = discounts == null ? Collections.emptyList() : Collections.unmodifiableList(discounts);
    }

    /**
     * Calculates shopping cart price according to this instances' prices and discounts.
     * Assumes customer friendly zero price on unknown items, @see getItemsPrice.
     *
     * @param cart shopping cart
     * @return cart items price with discounts applied
     */
    public BigDecimal getPrice(ShoppingCart cart) {
        Map<String, Long> items = cart.getItems();
        BigDecimal cartPrice = getItemsPrice(items);
        BigDecimal totalDiscount = getTotalDiscount(items);

        cartPrice = cartPrice.subtract(totalDiscount);

        return cartPrice;
    }

    /**
     * Calculate total discount on the shopping cart items.
     * Since original problem has only discounts applicable to same type of items
     * and there is at most one discount per item type this uses a very straightforward algorithm.
     * <p>
     * In case multiple discounts are available for the same type of items greedy or NDFA-like algorithm could be used.
     *
     * @param items shopping cart items
     * @return total discount amount
     */
    public BigDecimal getTotalDiscount(Map<String, Long> items) {
        HashMap<String, Long> remainingItems = new HashMap<>(items);
        BigDecimal totalDiscount = BigDecimal.ZERO;
        for (Discount discount : discounts) {
            BigDecimal discountValue = getItemsPrice(discount.getFreeItems());
            while (discount.removeMatch(remainingItems)) {
                totalDiscount = totalDiscount.add(discountValue);
            }
        }
        return totalDiscount;
    }

    /**
     * Gets price of the items in a price to quantity map.
     * Assumes a customer friendly zero price on unknown items.
     *
     * @param items map of item name to quantity in a cart
     * @return items price
     */
    BigDecimal getItemsPrice(Map<String, Long> items) {
        BigDecimal cartPrice = BigDecimal.ZERO;
        for (Map.Entry<String, Long> item : items.entrySet()) {
            BigDecimal itemPrice = prices.getOrDefault(item.getKey(), BigDecimal.ZERO);
            cartPrice = cartPrice.add(itemPrice.multiply(BigDecimal.valueOf(item.getValue())));
        }
        return cartPrice;
    }

}
