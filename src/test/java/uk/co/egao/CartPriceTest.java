package uk.co.egao;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertThat;

public class CartPriceTest {
    private static final String LIME = "Lime";
    private static final String MELON = "Melon";
    private static final String BANANA = "Banana";
    private static final String APPLE = "Apple";
    private CartPrice cartPrices;

    @Before
    public void setup() {
        HashMap<String, BigDecimal> prices = new HashMap<>();
        prices.put(APPLE, BigDecimal.valueOf(0.35));
        prices.put(BANANA, BigDecimal.valueOf(0.2));
        prices.put(MELON, BigDecimal.valueOf(0.5));
        prices.put(LIME, BigDecimal.valueOf(0.15));
        cartPrices = new CartPrice(prices, Arrays.asList(
                new Discount("Melons - buy one get one free",
                        singletonMap("Melon", 2L), singletonMap(MELON, 1L)
                ),
                new Discount("Limes - three for the price of two",
                        singletonMap("Lime", 2L), singletonMap(LIME, 1L)
                )
        ));
    }

    private void assertSameValue(BigDecimal expected, BigDecimal actual) {
        assertThat(actual, Matchers.comparesEqualTo(expected));
    }

    @Test
    public void emptyItemsPrice() {
        assertSameValue(BigDecimal.ZERO, cartPrices.getItemsPrice(emptyMap()));
    }

    @Test
    public void unknownItemPrice() {
        assertSameValue(BigDecimal.ZERO, cartPrices.getItemsPrice(singletonMap("Orange", 10L)));
    }

    @Test
    public void simpleItemsPrice() {
        assertSameValue(BigDecimal.valueOf(3.5), cartPrices.getItemsPrice(singletonMap(APPLE, 10L)));
        assertSameValue(BigDecimal.valueOf(2), cartPrices.getItemsPrice(singletonMap(BANANA, 10L)));
        assertSameValue(BigDecimal.valueOf(1.5), cartPrices.getItemsPrice(singletonMap(LIME, 10L)));
        assertSameValue(BigDecimal.valueOf(5), cartPrices.getItemsPrice(singletonMap(MELON, 10L)));
    }

    @Test
    public void emptyCartPrice() {
        assertSameValue(BigDecimal.ZERO, cartPrices.getPrice(new ShoppingCart()));
    }

    @Test
    public void simpleCartPrice() {
        assertSameValue(
                BigDecimal.valueOf(0.9),
                cartPrices.getPrice(
                        new ShoppingCart(APPLE, APPLE, BANANA)
                )
        );
    }

    @Test
    public void getTotalDiscount() {
        assertSameValue(
                BigDecimal.valueOf(0.0),
                cartPrices.getTotalDiscount(
                        new ShoppingCart(MELON).getItems()
                )
        );

        assertSameValue(
                BigDecimal.valueOf(0.5),
                cartPrices.getTotalDiscount(
                        new ShoppingCart(MELON, MELON).getItems()
                )
        );

        assertSameValue(
                BigDecimal.valueOf(0.5),
                cartPrices.getTotalDiscount(
                        new ShoppingCart(MELON, MELON, MELON).getItems()
                )
        );

        assertSameValue(
                BigDecimal.valueOf(1),
                cartPrices.getTotalDiscount(
                        new ShoppingCart(MELON, MELON, MELON, MELON).getItems()
                )
        );

        assertSameValue(
                BigDecimal.valueOf(0.15),
                cartPrices.getTotalDiscount(
                        new ShoppingCart(LIME, LIME, LIME).getItems()
                )
        );

        assertSameValue(
                BigDecimal.valueOf(0.65),
                cartPrices.getTotalDiscount(
                        new ShoppingCart(APPLE, APPLE, BANANA, LIME, LIME, LIME, MELON, MELON).getItems()
                )
        );
    }

    @Test
    public void getPriceWithDiscounts() {
        assertSameValue(
                BigDecimal.valueOf(0.5),
                cartPrices.getPrice(
                        new ShoppingCart(MELON)
                )
        );

        assertSameValue(
                BigDecimal.valueOf(0.5),
                cartPrices.getPrice(
                        new ShoppingCart(MELON, MELON)
                )
        );

        assertSameValue(
                BigDecimal.valueOf(1),
                cartPrices.getPrice(
                        new ShoppingCart(MELON, MELON, MELON)
                )
        );

        assertSameValue(
                BigDecimal.valueOf(1),
                cartPrices.getPrice(
                        new ShoppingCart(MELON, MELON, MELON, MELON)
                )
        );

        assertSameValue(
                BigDecimal.valueOf(0.3),
                cartPrices.getPrice(
                        new ShoppingCart(LIME, LIME, LIME)
                )
        );

        assertSameValue(
                BigDecimal.valueOf(1.7),
                cartPrices.getPrice(
                        new ShoppingCart(APPLE, APPLE, BANANA, LIME, LIME, LIME, MELON, MELON)
                )
        );
    }
}