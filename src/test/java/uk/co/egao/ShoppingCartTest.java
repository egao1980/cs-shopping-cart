package uk.co.egao;

import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple shopping cart app.
 */
public class ShoppingCartTest {
    /**
     *
     */
    @Test
    public void emptyCartValue() {
        assertEquals(Collections.<String, Long>emptyMap(), new ShoppingCart().getItems());
    }

    @Test
    public void cartItems() {
        assertEquals(singletonMap("Apple", 1L), new ShoppingCart("Apple").getItems());
        assertEquals(singletonMap("Apple", 2L), new ShoppingCart("Apple", "Apple").getItems());
        HashMap<String, Long> items = new HashMap<>();
        items.put("Apple", 2L);
        items.put("Melon", 1L);
        assertEquals(items, new ShoppingCart("Apple", "Apple", "Melon").getItems());
    }

    @Test
    public void addRemoveCartItems() {
        HashMap<String, Long> items = new HashMap<>();
        items.put("Apple", 2L);
        items.put("Melon", 1L);
        ShoppingCart cart = new ShoppingCart("Apple", "Apple", "Melon");

        assertEquals(items, cart.getItems());

        cart.removeItem("Apple", 2L);

        items.put("Apple", 0L);
        assertEquals(items, cart.getItems());

        cart.addItem("Melon", 1L);
        items.put("Melon", 2L);
        assertEquals(items, cart.getItems());

    }
}
