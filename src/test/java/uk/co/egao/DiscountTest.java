package uk.co.egao;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static org.junit.Assert.*;

public class DiscountTest {

    private static final String MELON = "Melon";
    private static final String LIME = "Lime";
    private static final String APPLE = "Apple";
    private Discount twoForOne;
    private Discount threeForTwo;

    @Before
    public void setup() {
        twoForOne = new Discount("Melons - two for price of one", singletonMap(MELON, 2L), singletonMap(MELON, 1L));
        threeForTwo = new Discount("Limes - three for price of two", singletonMap(LIME, 3L), singletonMap(LIME, 1L));
    }

    private Map<String, Long> items(String... items) {
        return Arrays.stream(items).collect(groupingBy(identity(), counting()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void undefinedDiscount() {
        new Discount("undefined", Collections.emptyMap(), emptyMap());
    }

    @Test(expected = IllegalArgumentException.class)
    public void unbundledFreeItemsDiscount() {
        new Discount("unbundled", singletonMap(APPLE, 2L), singletonMap(LIME, 1L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void tooManyFreeItemsDiscount() {
        new Discount("too many", singletonMap(APPLE, 2L), singletonMap(APPLE, 3L));
    }

    @Test
    public void noMatchEmpty() {
        assertFalse(twoForOne.hasMatch(Collections.emptyMap()));
        assertFalse(threeForTwo.hasMatch(Collections.emptyMap()));
    }

    @Test
    public void hasMatch() {
        assertFalse(twoForOne.hasMatch(items(MELON)));
        assertFalse(twoForOne.hasMatch(items(MELON, LIME)));
        assertTrue(twoForOne.hasMatch(items(MELON, MELON)));

        assertFalse(threeForTwo.hasMatch(items(MELON)));
        assertFalse(threeForTwo.hasMatch(items(MELON, LIME)));
        assertFalse(threeForTwo.hasMatch(items(LIME, LIME)));
        assertFalse(threeForTwo.hasMatch(items(LIME, LIME, MELON)));
        assertTrue(threeForTwo.hasMatch(items(LIME, LIME, LIME)));
        assertTrue(threeForTwo.hasMatch(items(LIME, LIME, LIME, LIME)));
    }

    @Test
    public void removeMatch() {
        assertFalse(twoForOne.removeMatch(items(APPLE)));
        Map<String, Long> items = new HashMap<>(singletonMap(MELON, 2L));
        assertTrue(twoForOne.removeMatch(items));
        assertEquals(0L, (long) items.get(MELON));

        items = new HashMap<>(singletonMap(MELON, 3L));
        assertTrue(twoForOne.removeMatch(items));
        assertEquals(1L, (long) items.get(MELON));
    }
}